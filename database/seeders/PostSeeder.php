<?php

namespace Database\Seeders;

use App\Models\Like;
use App\Models\Post;
use App\Models\Rekomendasi;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // post 1

        $post = Post::create([
            'id_kategori' => '1',
            'id_user' => 2,
            'sampul' => 'post1.jpg',
            'judul' => 'Ini Adalah 4 Musisi Folk Indonesia Masa Kini',
            'konten' => '<p>Musik folk menjadi salah satu favorit banyak orang. Meski biasanya lirik hingga komposisi musiknya cenderung dibungkus secara sederhana, musik folk nyatanya begitu mudah untuk diresapi. Mereka yang menyukai jenis musik seperti ini biasanya mencintai kedamaian dan tidak suka berbelit-belit.</p>

                        <p>Indonesia sudah banyak musisi folk seperti senior Ebit G. Ade, Iwan Fals dan masih banyak lagi. Kali ini akan membahas 5 musisi folk indie yang karya-karyanya banyak menggambarkan kondisi sosial masa kini. Siapa saja mereka?</p>

                        <p>1.Amigdala</p>

                        <p>Band asal Bandung yang digawangi oleh 4 personel ini populer berkat lagu baru mereka bertajuk &ldquo;Ku Kira Kau Rumah&rdquo;. Menariknya, banyak orang yang menyebut bahwa Amigdala terdengar seperti Banda Neira yang telah bubar balik kanan. Meski hadir dengan gayanya sendiri, tak bisa dipungkiri bahwa vokal Andari dari Amigdala hampir sama dengan Rara Sekar, Banda Neira.</p>

                        <p>2. Banda Neira</p>

                        <p>Petikan gitar Ananda Badudu dirasa cocok dengan suara lembut khas Rara Sekar dalam grup Banda Neira. Keduanya telah melahirkan sejumlah lagu yang mendammaikan hati. Berawal dari proyek &lsquo;iseng-iseng&rsquo; berhadiah, Banda Neira bahkan telah menelurkan dua buah album hingga saat ini. &ldquo;Yang Patah Tumbuh, Yang Hilang Berganti&rdquo; menjadi album kedua mereka yang dirilis 2016 lalu.&nbsp; Sayangnya, Ananda Badudu dan Rara Sekar sepakat untuk mengakhiri Banda Neira dan berjalan masing-masing.</p>

                        <p>3. Nosstress</p>

                        <p>Grup indie folk yang satu ini berasal dari Bali yang banyak bercerita tentang anomali cuaca serta efek perubahan iklim dalam karya-karya mereka. Semua lagu mereka dikemas dengan sangat sederhana dan sangat mudah ditangkap esensinya. Contohnya saja &ldquo;Seperti Dia&rdquo; yang mengisahkan tentang hujan yang sering terlambat datang, dan sekalinya datang membuat manusia kesusahan. Lirik semacam itu yang mereka hadirkan dalam beberapa lagu mereka.</p>

                        <p>4. Jason Ranti</p>

                        <p>Jason Ranti merupakan musisi asal Tangerang yang telah merilis album debutnya bertajuk &ldquo;Akibat Pergaulan Blues&rdquo; pada 2017 lalu. Jason Ranti memilih jalur musik sebagai ajang berkarya bukan tanpa alasan, ia bahkan mengaku bahwa sang ibunda lah yang mengenalkannya pada musik-musik bagus dan bacaan. Sebelumnya, ia bahkan pernah menjadi vokalis band Stairway to Zinna. Sejak kemunculannya, Jason mengaku tak pernah berambisi untuk menjadi terkenal melainkan ingin terus membuat karya yang bagus.</p>
',
            'slug' => Str::slug('ini-adalah-4-musisi-folk-indonesia-masa-kini')
        ]);

        DB::table('post_tag')->insert([
            'id_post' => $post->id,
            'id_tag' => 1
        ]);

        Like::create([
            'id_post' => $post->id,
            'id_user' => 2
        ]);

        Rekomendasi::create([
            'id_post' => $post->id
        ]);

        // post 2

        $post = Post::create([
            'id_kategori' => '2',
            'id_user' => 2,
            'sampul' => 'post2.jpg',
            'judul' => 'Resep Membuat Dumpling Daging Ayam',
            'konten' => '<p>Bahan-bahan dan Bumbu:</p>

                    <ol>
                        <li>Siapkan 150 gr ayam cincang/ giling</li>
                        <li>Sediakan 4 ekor udang peci (kuliti dan cincang halus)</li>
                        <li>Sediakan 2 bungkus kulit dumpling&nbsp;siap pakai</li>
                        <li>Siapkan 1 sdm tepung terigu</li>
                        <li>Gunakan 1 buah telur ayam</li>
                        <li>Gunakan 2 batang daun bawang (iris2)</li>
                        <li>Siapkan 1&frasl;4 buah kol kecil / kubis (cincang)</li>
                        <li>Sediakan 1 SDM minyak sayur</li>
                        <li>Ambil Tambahan saat ungkep dumpling</li>
                        <li>Siapkan 50 ml air matang</li>
                        <li>Sediakan 1 SDM tepung terigu (utk di campur di air)</li>
                        <li>Gunakan Bumbu:</li>
                        <li>Gunakan 1&frasl;2 ruas jari Jahe (parut)</li>
                        <li>Sediakan 3 buah bawang putih (cincang)</li>
                        <li>Ambil 1 sdt lada bubuk</li>
                        <li>Ambil 1 sdt saos tiram</li>
                        <li>Ambil 1 SDM kecap asin</li>
                        <li>Gunakan 1 sdt kecap Inggris</li>
                        <li>Siapkan 1 SDM minyak wijen</li>
                        <li>Siapkan Secukupnya garam dan gula</li>
                    </ol>

                    <p>Langkah-Langkah:</p>

                    <ol>
                        <li>Campur semua bahan basah dalam satu wadah</li>
                        <li>Tambahkan bahan2 bumbu lalu aduk hingga tercampur rata</li>
                        <li>Siapkan kulit dumpling isi dengan isian di atas +- 1 sendok teh, tergantung seberapa besar kulit dumpling</li>
                        <li>Bentuk setengah bulatan lalu di rekatkan dengan air, dibuat seperti Dumpling dengan cara di cubit2 membentuk seperti kipas. (Buat hingga kulit dumpling&nbsp;habis)</li>
                        <li>Panaskan teflon, tuangkan minyak sayur 1 SDM, masukan dumpling satu persatu hingga teflon penuh, diamkan +- 5 menit hingga bawah dumpling sedikit kering dan matang kecoklatan.</li>
                        <li>Campur 1&frasl;2 gelas air +- 50 ml dengan 1 SDM tepung terigu, siramkan pada dumpling&nbsp;di dalam teflon. tutup teflon seperti di ungkep</li>
                        <li>Angkat +- 5 menit setelah di ungkep.. cairan ini gunanya buat bikin lapisan crunchy di bawah dampling&nbsp;nya.&nbsp;ini enak bgt kalo makannya masih panas2.</li>
                        <li>Sajikan dengan kecap asin yg di kasih cabe rawit/ sambel asem manis seperti sambel Bangkok.</li>
                    </ol>

                    <p>Silahkan mencobanya..</p>

                    <p>&nbsp;</p>',
            'slug' => Str::slug('resep-membuat-dumpling-daging-ayam')
        ]);

        DB::table('post_tag')->insert([
            'id_post' => $post->id,
            'id_tag' => 4
        ]);

        Like::create([
            'id_post' => $post->id,
            'id_user' => 3
        ]);

        Rekomendasi::create([
            'id_post' => $post->id
        ]);

        // post 3

        $post = Post::create([
            'id_kategori' => '4',
            'id_user' => 1,
            'sampul' => 'post3.jpg',
            'judul' => 'Apa Sih Manfaat jahe Untuk Kesehatan tubuh?',
            'konten' => '<p style="font-style: italic;">1. Mencegah kanker ovarium</p>

                        <p style="font-style: italic;">Kandungan senyawa dalam jahe telah diteliti dapat mencegah kanker ovarium. Sebuah penelitian dalam jurnal &ldquo;BMC Complementary Medicine and Therapies&rdquo; menunjukkan bahwa manfaat jahe dapat menghentikan pertumbuhan sel kanker ovarium. Dalam penelitian, jahe efektif dalam mengendalikan peradangan yang berkontribusi pada perkembangan sel kanker ovarium. Para peneliti menyimpulkan, jahe juga dapat menghentikan pertumbuhan sel kanker dengan menghentikan reaksi peradangan.</p>

                        <p style="font-style: italic;">2.&nbsp;Meredakan batuk</p>

                        <p style="font-style: italic;">Manfaat jahe selanjutnya adalah untuk meredakan batuk. Studi dalam &ldquo;American Journal of Respiratory Cell and Molecular Biology&rdquo; menyimpulkan, beberapa senyawa anti-inflamasi dalam jahe dapat mengendurkan selaput di saluran udara sehingga meredakan batuk. Jahe membantu mengurangi iritasi di tenggorokan dan paru-paru, serta membersihkan saluran udara dan meredakan batuk kering. Agar optimal, konsumsi jahe dengan menyeduhnya dalam minuman hangat. Anda juga dapat menambahkan madu untuk meningkatkan rasa dan khasiat.&nbsp;</p>

                        <p style="font-style: italic;">3.&nbsp;Meredakan sakit saat menstruasi</p>

                        <p style="font-style: italic;">Sakit saat menstruasi sering dialami oleh sebagian wanita. Manfaat jahe secara signifikan dapat mengurangi intensitas dan durasi nyeri, menurut studi dalam &ldquo;BMC Complementary Medicine and Therapies&rdquo;. Peneliti menyimpulkan, konsumsi 1.500&thinsp;mg bubuk jahe setiap hari selama tiga hari adalah cara yang aman dan efektif untuk meredakan sakit saat menstruasi. Jahe menurunkan kadar prostaglandin, yaitu hormon penyebab rasa sakit. Konsumsi jahe juga membantu melawan kelelahan yang terkait dengan sindrom pramenstruasi dan dapat membuat menstruasi yang tidak teratur menjadi teratur. DLL<br />
                        <br />
                        &nbsp;</p>',
            'slug' => Str::slug('apa-sih-manfaat-jahe-untuk-kesehatan-tubuh-?')
        ]);

        DB::table('post_tag')->insert([
            'id_post' => $post->id,
            'id_tag' => 9
        ]);

        DB::table('post_tag')->insert([
            'id_post' => $post->id,
            'id_tag' => 10
        ]);

        Like::create([
            'id_post' => $post->id,
            'id_user' => 3
        ]);

        Rekomendasi::create([
            'id_post' => $post->id
        ]);

        // post 4

        $post = Post::create([
            'id_kategori' => '3',
            'id_user' => 2,
            'sampul' => 'post4.jpg',
            'judul' => 'Tips Membuat Vas Dari Botol Bekas',
            'konten' => '<p><strong>Bahan dan Peralatan yang Dibutuhkan:</strong></p>

                        <ul>
                            <li>Botol plastik bekas</li>
                            <li>Pisau</li>
                            <li>Hiasan untuk mata</li>
                            <li>Pupuk/tanah untuk menanam</li>
                            <li>Tanaman</li>
                        </ul>

                        <p><strong>Cara Membuat Kerajinan Tangan dari Botol Plastik Bekas&nbsp;Berbentuk Pot Bunga:</strong></p>

                        <ol>
                            <li>Siapkan bahan dan peralatan yang dibutuhkan.</li>
                            <li>Setelah semua sudah terkumpul, langkah selanjutnya adalah memotong botol plastik dengan pisau.</li>
                            <li>Ukurannya dikira-kira saja atau lihat pada gambar.</li>
                            <li>Beri sedikit hiasan sesuai keinginan. Jika ingin sesuai contohnya, tempelkan saja tutup botolnya sebagai mulut&nbsp;dan tambahkan mata boneka, lalu tempel.</li>
                            <li>Jangan lupa untuk melubangi bagian bawah botol agar air tidak mengendap di dalam botol.</li>
                            <li>Masukkan botol pupuk dan tanah ke dalam botol.</li>
                            <li>Masukkan bibit tanaman yang ingin ditanam.</li>
                        </ol',
            'slug' => Str::slug('tips-membuat-vas-dari-botol-bekas')
        ]);

        DB::table('post_tag')->insert([
            'id_post' => $post->id,
            'id_tag' => 6
        ]);

        Like::create([
            'id_post' => $post->id,
            'id_user' => 3
        ]);


        // post 5

        $post = Post::create([
            'id_kategori' => '1',
            'id_user' => 1,
            'sampul' => 'post5.jpg',
            'judul' => 'Yuk Cari Tahu Apa Ciri Khas Genre Musik R&B',
            'konten' => 'p>R&amp;B memiliki ciri alunan musik yang lebih enak di dengar. R&amp;B lebih dekat dengan pop, jazz, dan soul. Genre ini lebih menitik beratkan pada kualitas vokal yang baik dan lirik yang lebih santai. R&amp;B cenderung lebih mengutamakan improvisasi melodi, khususnya vocal para penyanyi dengan harmonisasi yang progresif.</p>

                        <p>Teknik vokal seorang penyanyi seperti riffs and runs akan sangat berpengaruh dalam pembawaan sebuah lagu R&amp;B. Improvisasi riffs and runs seorang penyanyi akan melengkapi suatu lagu serta membuat lagu tersebut menjadi lebih berwarna dan unik.</p>

                        <p>Alat musik yang biasanya digunakan dalam genre musik ini adalah : drum kit, double bass, saxophone, horns, piano, organ, electric guitar, vocals and synthesizers, keyboard, drum machine, dan synclavier.</p>',
            'slug' => Str::slug('apa-ciri-khas-genre-musik-r&b')
        ]);

        DB::table('post_tag')->insert([
            'id_post' => $post->id,
            'id_tag' => 3
        ]);

        Like::create([
            'id_post' => $post->id,
            'id_user' => 3
        ]);

        // post 6

        $post = Post::create([
            'id_kategori' => '4',
            'id_user' => 1,
            'sampul' => 'post6.jpg',
            'judul' => 'Tips Bagaimana Squat Yang Benar',
            'konten' => '<p>Begini cara melakukan squat yang benar.</p>

                        <ol>
                            <li>Awali dengan posisi berdiri tegak.</li>
                            <li>Berdiri dengan kaki dibuka selebar pinggul</li>
                            <li>Turunkan tubuh Anda sejauh yang Anda bisa dengan mendorong punggung ke belakang, sambil naikkan lengan Anda lurus ke depan untuk menjaga keseimbangan.</li>
                            <li>Tubuh bagian bawah harus sejajar dengan lantai dan dada harus dibusungkan, tidak membungkuk. Lalu angkat sebentar dan kembali ke posisi awal.</li>
                            <li>Saat Anda menurunkan tubuh Anda seperti ingin duduk atau jongkok, paha belakang memanjang di sendi pinggul dan memperpendek sendi lutut.</li>
                            <li>Pada saat yang sama, otot punggung atas menegang, yang membantu Anda mempertahankan batang tubuh Anda tetap tegak, sehingga punggung Anda tidak berputar.</li>
                        </ol>',
            'slug' => Str::slug('tips-bagaimana-squat-yang-benar')
        ]);

        DB::table('post_tag')->insert([
            'id_post' => $post->id,
            'id_tag' => 8
        ]);

        Like::create([
            'id_post' => $post->id,
            'id_user' => 3
        ]);


        // post 7

        $post = Post::create([
            'id_kategori' => '2',
            'id_user' => 2,
            'sampul' => 'post7.jpg',
            'judul' => 'Yuk Cari Tahu Apa Perbedaan Macaron dan Macaroon',
            'konten' => '<p>Sebenarnya macaron dan macaroon adalah dua jenis panganan manis yang memiliki bentuk mirip. Bahan bakunya pun sama, yaitu putih telur. Tetapi ada perbedaan mendasar di antara keduanya.</p>

                        <p>Dilansir Real Simple, macaron dan macaroon merupakan &#39;kerabat jauh&#39;. Menurut Chef Jansen Chan, Director of Pastry Operations di International Culinary Center, kue macaron dan macaroon memiliki akar kata yang sama, namun berkembang merujuk pada dua hal berbeda.</p>

                        <p>Macaron atau yang disebut juga Parisian Macarons atau Macarons Gerbet adalah kue yang dibuat dari tepung almond, putih telur, dan gula. Diproses dengan cara dipanggang hingga menghasilkan tekstur luar yang tipis dan bagian dalam yang lembut. Macaron biasanya disajikan bertangkup seperti sandwich (roti lapis). Bagian tengahnya diisi selai atau butter cream (krim mentega).</p>

                        <p>Sementara itu, macaroon terbuat dari putih telur, kelapa, dan gula yang dipanggang hingga berwarna cokelat keemasan. Bagian dalamnya terasa padat namun kenyal.Macaroon paling cocok bila disajikan dengan teh atau kopi hangat.Nama lain macaroon adalah congolais dan sangat terkenal di Prancis. Chef Jansen juga mengatakan kalau macaron maupun macaroon merupakan kuliner yang dipengaruhi oleh budaya Prancis dan Italia.</p>

                        <p>Jadi, mulai sekarang jangan sampai salah membedakan macaron dan macaroon, ya&hellip;</p>',
            'slug' => Str::slug('yuk-cari-tahu-apa-perbedaan-macaron-dan-macaroon')
        ]);

        DB::table('post_tag')->insert([
            'id_post' => $post->id,
            'id_tag' => 10
        ]);

        Like::create([
            'id_post' => $post->id,
            'id_user' => 3
        ]);
    }
}