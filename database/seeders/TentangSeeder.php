<?php

namespace Database\Seeders;

use App\Models\Tentang;
use Illuminate\Database\Seeder;

class TentangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tentang::create([
            'konten' => '<p>Haloo Kawann..<br>AB blog ini berisi tentang musik,kesehatan,kerajinan,dan resep makanan..</p><p>Semoga kalian menyukai artikel-artikel di dalamnya..</p><p>Terima Kasih</p>',
            'instagram' => 'www.instagram.com',
            'twitter' => 'www.twitter.com'
        ]);
    }
}