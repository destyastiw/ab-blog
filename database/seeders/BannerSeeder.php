<?php

namespace Database\Seeders;

use App\Models\Banner;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Banner::create([
            'sampul' => 'banner1.jpg',
            'judul' => 'Manfaat Sayur Hijau Untuk Tubuh',
            'konten' => '<p><strong>1. Menjaga Kesehatan Sistem Pencernaan</strong></p><p>Kandungan serat pada sayuran hijau mampu menyerap tambahan air dalam usus, dan juga menjaga keberadaan bakteri sehat. Brokoli, kacang hijau, lobak hijau, dan apel adalah contoh sayuran hijau yang baik untuk system pencernaan.</p><p><strong>2. Menjaga Kesehatan Mata</strong></p><p>Kandungan lutein dan zeaxanthin pada bayam, kiwi, dan anggur mampu mencegah munculnya katarak pada mata kita. Tentu saja dengan diimbangi dengan mengkonsumsi vitamin A maka kesehatan mata akan lebih terjaga.&nbsp;</p><p><strong>3. Mencegah Penyakit Jantung</strong></p><p>Kandungan vitamin K pada brokoli dan Asparagus sangat berguna bagi kesehatan jantung. Kurangnya vitamin K dapat mengganggu peredaran darah ke jantung. Apabila kamu membenci kedua sayuran tersebut, kamu bisa mengakalinya dengan membuatnya sebagai campuran pada <i>chicken soup</i>.&nbsp;</p><p><strong>4. Mencegah kanker</strong></p><p>&nbsp;Banyak penelitian mencatat bahwa diet kaya sayuran dan buah-buahan hijau dapat membantu menangkal kanker. Mereka mengandung fitonutrien seperti antioksidan, karotenoid dan flavonoid yang membantu dalam memerangi kanker perut, kanker usus besar, kanker kulit dan kanker payudara. Fitonutrien ini ditemukan dalam avokad, zaitun, apel hijau, bayam, kale dan lainnya.&nbsp;</p><p>ini adalah 4 manfaat sayur hijau untuk kesehatan.Keep Health!!!<br>&nbsp;</p>',
            'slug' => Str::slug('manfaat-sayur-hijau-untuk-tubuh')
        ]);

        Banner::create([
            'sampul' => 'banner2.jpg',
            'judul' => 'Cara Membuat Tas Dengan Memanfaatkan Serat Alam',
            'konten' => '<p>Alat dan Bahan yang dibutuhkan :&nbsp;</p><ul><li>Daun Pandan</li><li>Gunting</li><li>Kertas karton</li><li>pensil</li><li>Benang</li><li>Jarum</li><li>Pernak-pernik seperti kancing, mata, pita dan lain-lain</li></ul><p>Cara Membuatnya :&nbsp;</p><ol><li>Siapkan daun pandan yang telah dipanen dan dibuang duri-duri nya</li><li>Lalu, potong daun pandan sesuai dengan ukuran anyaman. Potongan daun pandan tersebut lalu direbus selama 30 menit yang bertujuan untuk menghilangkan getah daunnya.</li><li>Seteleh 30 menit direbus, selanjutnya adalah menjemur daun pandan ditempat yang sejuk (Jangan terkena sinar matahari langsung.</li><li>Diamkan daun pandan selama 6 jam , lalu rendamlah daun pandan dengan air&nbsp; biasa selama 4 jam.</li><li>Baru dijemur daun pandan dibawah sinar matahari hingga berwarna keputihan.</li><li>Warnai daun pandan sesuai dengan selera anda.</li><li>Anyamlah daun pandan sesuai dengan pola tas yang sudah ditentukan.</li><li>Jangan lupa tambahakan aksesoris pelengkap seperti tali, kacing, mata, pita dan lain-lain</li></ol><p>Sangat mudah kan membuat tas anyaman daun pandan, anda pun dapat mencobanya sendiri dirumah. Tas yang dihasilkan pun dapat anda gunakan sendiri maupun untuk dijual. Selain itu anda dapat menjadikan tas anyam pandan sebagai usaha kerajinan anda sendiri.</p><p>Demikianlah ulasan mengenai cara membuat tas anyam pandan yang dapat disampaikan, semoga bermanfaat bagi kalian semua. Terimakasih</p>',
            'slug' => Str::slug('cara-membuat-tas-dengan-memanfaatkan-serat-alam')
        ]);

        Banner::create([
            'sampul' => 'banner3.jpg',
            'judul' => 'Apa Itu Olahraga Kardio',
            'konten' => '<p>Olahraga kardio adalah salah satu aerobik yang berguna untuk kesehatan jantung. Contoh olahraga kardio di rumah seperti bersepeda, berlari, dan peregangan. Berikut penjelasan manfaat olahraga kardio. Olahraga kardio adalah jenis latihan aerobik yang bermanfaat meningkatkan detak jantung, membakar lemak, dan kalori. Olahraga kardio ini terdiri dari berbagai macam dan bisa digunakan untuk pemula. Tujuan dari kardio adalah meningkatkan kesehatan organ tubuh seperti paru-paru dan jantung. Jika Anda sering berlatih kardio, olahraga ini bisa menjaga pernapasan dan tekanan darah. Contoh olahraga kardio yaitu bersepeda, berenang, dan berjalan. Tak hanya olahraga, mengepel lantai atau membersihkan rumah termasuk latihan dari kardio.</p>',
            'slug' => Str::slug('apa-itu0olahraga-kardio')
        ]);
    }
}