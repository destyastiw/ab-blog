<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = ['Genre Folk', 'Genre Pop', 'Genre R&B', 'Resep Masakan Asian', 'Resep Masakan Eropa', 'Kerajinan Bahan Bekas', 'Kerajinan Serat Alam', 'Olahraga', 'Makanan', 'Minuman'];

        foreach ($tags as $tag) {
            Tag::create([
                'nama' => $tag,
                'slug' => Str::slug($tag)
            ]);
        }
    }
}